from flask_wtf import Form, FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Email
from wtforms.widgets import PasswordInput


# class AuthForm(FlaskForm):
class AuthForm(Form):
    email = StringField('email', validators=[Email()])
    password = StringField('pass', widget=PasswordInput(), validators=[DataRequired()])
    submit = SubmitField('[Отправить]')
