from flask import flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required, login_user, logout_user

from app.email import send_email
from .. import db
from app.auth.models.user import User
from .models.address import Address

from . import auth
from .forms import LoginForm, RegistrationForm, ChangePasswordForm, \
    PasswordResetRequestForm, PasswordResetForm, EditProfileForm, \
    ChangeEmailForm, AddressForm


# @auth.before_app_request
# def before_request():
#     if current_user.is_authenticated \
#             and not current_user.confirmed \
#             and request.endpoint \
#             and request.endpoint[:5] != 'auth.' \
#             and request.endpoint != 'static':
#         return redirect(url_for('auth.unconfirmed'))


@auth.route('/login/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('auth.profile'))
        flash('Неправильный адрес почты или пароль')
    return render_template('auth/login.html', form=form, title='Вход в аккаунт')


@auth.route('/logout/')
@login_required
def logout():
    logout_user()
    flash('Вы вышли из аккаунта')
    return redirect(url_for('main.index'))


@auth.route('/register/', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(email=form.email.data, password=form.password.data)
        db.session.add(user)
        db.session.commit()
        login_user(user)

        token = user.generate_confirmation_token()
        send_email(user.email, 'Подтвердите ваш почтовый адрес',
                   'auth/email/confirm', user=user, token=token)
        flash('Аккаунт создан, письмо с его подтверждением отправлено на ваш почтовый адрес')
        return redirect(url_for('auth.profile'))
    return render_template('auth/form.html', form=form, title='Регистрация')


@auth.route('/confirm/<token>/')
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect(url_for('auth.profile'))
    if current_user.confirm(token):
        flash('Вы подтвердили свой почтовый адрес')
    else:
        flash('Подтверждение недействительно или срок действия истёк')
    return redirect(url_for('auth.profile'))


@auth.route('/confirm/')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    send_email(current_user.email, 'Подтвердите ваш почтовый адрес',
               'auth/email/confirm', user=current_user, token=token)
    flash('Письмо с подтверждением отправлено на ваш почтовый адрес')
    return redirect(url_for('auth.profile'))


@auth.route('/change-password/', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.password.data
            db.session.add(current_user)
            flash('Пароль изменен')
            return redirect(url_for('auth.profile'))
        else:
            flash('Неправильно введен заменяемый пароль')
    return render_template("auth/form.html", form=form, title='Изменение пароля')


@auth.route('/reset/', methods=['GET', 'POST'])
def password_reset_request():
    if not current_user.is_anonymous:
        return redirect(url_for('auth.profile'))
    form = PasswordResetRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            token = user.generate_confirmation_token()
            send_email(user.email, 'Восстановление пароля',
                       'auth/email/reset_password',
                       user=user, token=token,
                       next=request.args.get('next'))
        flash('Инструкция по восстановлению пароля отправлена на ваш почтовый ящик')
        return redirect(url_for('auth.login'))
    return render_template('auth/form.html', form=form, title='Восстановление пароля')


@auth.route('/reset/<token>/', methods=['GET', 'POST'])
def password_reset(token):
    if not current_user.is_anonymous:
        return redirect(url_for('main.index'))
    form = PasswordResetForm()
    if form.validate_on_submit():
        if User.reset_password(token, form.password.data):
            flash('Ваш пароль был изменен')
            return redirect(url_for('auth.login'))
        else:
            return redirect(url_for('main.index'))
    return render_template('auth/form.html', form=form, title='Восстановление пароля')


@auth.route('/change-email/', methods=['GET', 'POST'])
@login_required
def change_email():
    form = ChangeEmailForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            new_email = form.email.data
            token = current_user.generate_email_change_token(new_email)
            send_email(new_email, 'Подтвердите изменение почтового ящика',
                       'auth/email/change_email',
                       user=current_user, token=token)
            flash('Письмо с инструкциями отправлено на новый почтовый адрес')
            return redirect(url_for('auth.profile'))
        else:
            flash('Неправильный адрес почтового ящика или пароль')
    return render_template("auth/form.html", form=form, title='Изменение почтового ящика')


@auth.route('/change-email/<token>/')
@login_required
def change_email_token(token):
    if current_user.change_email(token):
        flash('Адрес почтового адреса был обновлён')
    else:
        flash('Недопустимый запрос')
    return redirect(url_for('auth.profile'))


@auth.route('/profile/')
@login_required
def profile():
    return render_template('profile.html')


@auth.route('/profile/edit/', methods=['GET', 'POST'])
@login_required
def profile_edit():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.surname = form.surname.data
        current_user.name = form.name.data
        current_user.middle_name = form.middle_name.data
        current_user.telephone = form.telephone.data
        db.session.add(current_user)
        flash('Контактные данные вашего профиля обновлены')
        return redirect(url_for('auth.profile'))

    if request.method == 'GET':
        form.surname.data = current_user.surname
        form.name.data = current_user.name
        form.middle_name.data = current_user.middle_name
        form.telephone.data = current_user.telephone
    return render_template('auth/form.html', form=form, title='Редактирование контактных данных')


@auth.route('/address/add/', methods=['GET', 'POST'])
@login_required
def address_add():
    form = AddressForm()
    if form.validate_on_submit():
        db.session.add(
            Address(
                country=form.country.data,
                region=form.region.data,
                city=form.city.data,
                address=form.address.data,
                index=form.index.data,
                user_id=current_user.id
            ))
        flash('Новый адрес добавлен')
        return redirect(url_for('auth.profile'))
    return render_template('auth/form.html', form=form, title='Добавление адреса')


@auth.route('/address/<address_id>/delete/', methods=['GET'])
@login_required
def address_delete(address_id):
    address = Address.query.get(int(address_id))
    if address.user_id == current_user.id:
        Address.query.filter_by(id=address.id).delete()
        flash('Адрес удалён')
    return redirect(url_for('auth.profile'))


@auth.route('/address/<address_id>/edit/', methods=['GET', 'POST'])
@login_required
def address_edit(address_id):
    address = Address.query.get(int(address_id))
    if address.user_id != current_user.id:
        return redirect(url_for('auth.profile'))

    form = AddressForm()
    if form.validate_on_submit():
        address.country = form.country.data
        address.region = form.region.data
        address.city = form.city.data
        address.address = form.address.data
        address.index = form.index.data

        db.session.add(address)
        flash('Адрес обновлён')
        return redirect(url_for('auth.profile'))

    if request.method == 'GET':
        form.country.data = address.country
        form.region.data = address.region
        form.city.data = address.city
        form.address.data = address.address
        form.index.data = address.index
    return render_template('auth/form.html', form=form, title='Редактирование адреса')
