from flask import current_app
from flask import flash
from flask.ext.login import AnonymousUserMixin
from flask_login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from werkzeug.security import generate_password_hash, check_password_hash

from ... import db, login_manager


class Role:
    consumer = 0b0001
    provider = 0b0010
    operator = 0b0100
    moderator = 0b1000
    administrator = 0b01000000
    superuser = 0b10000000


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)

    email = db.Column(db.String(100), unique=True, index=True)
    confirmed = db.Column(db.Boolean, default=False)

    password_hash = db.Column(db.String(128))
    role = db.Column(db.Integer, default=0)

    telephone = db.Column(db.String(20), nullable=True)

    surname = db.Column(db.String(100), nullable=True)
    name = db.Column(db.String(100), nullable=True)
    middle_name = db.Column(db.String(100), nullable=True)

    addresses = db.relationship('Address', backref='role', lazy='dynamic')

    def __init__(self, email=None, password=None):
        self.email, self.password = email, password

    @property
    def password(self):
        raise AttributeError('пароль не может быть прочитан')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})

    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    @staticmethod
    def reset_password(token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            flash('Подтверждение недействительно или срок действия истёк')
            return False
        user = User.query.get(int(data.get('confirm')))
        user.password = new_password
        db.session.add(user)
        return True

    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def change_email(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        return True

    def can(self, role=None):
        r = role if role else Role.administrator
        if self.role and (self.role & Role.superuser) == Role.superuser:
            return True
        return self.role and (self.role & r) == r

    def __repr__(self):
        return '<User {}>'.format(self.email)


class AnonymousUser(AnonymousUserMixin):
    def can(self, permissions=None):
        return False


login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
