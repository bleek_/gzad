from ... import db


class Address(db.Model):
    __tablename__ = 'addresses'
    id = db.Column(db.Integer, primary_key=True)

    country = db.Column(db.String(40))
    region = db.Column(db.String(40))
    city = db.Column(db.String(40))
    address = db.Column(db.String(100))
    index = db.Column(db.String(10))

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __repr__(self):
        return '<Address {}>'.format(self.data)