from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField, PasswordField
from wtforms.validators import DataRequired, Email, Length, Regexp, EqualTo, ValidationError

from app.auth.models.user import User


class Validators:
    email = Regexp('^[^@]+@[^@.]+\.[^@]+$', 0, 'Адрес почтового ящика введен неверно')
    required = Regexp('^.+$', 0, 'Поле не может быть пустым')
    password = Regexp('^(?=.*[A-ZА-ЯЁ])(?=.*[!@#$%&*])(?=.*\d)(?=.*[a-zа-яё]).{6,}$',
                      0, 'Недостаточная сложность пароля или длина менее 6-ти')
    name = Regexp('^([a-zA-Zа-яА-ЯёЁ\"]([ _:,-]*[a-zA-Zа-яА-ЯёЁ0-9.?!\"]+)*)?$', 0,
                  'Название может содержать буквы, цифры и разделяться знаками: .,:-_')
    telephone = Regexp('^\+?\d*[\- ]?\(?\d*\)?[\- ]?[\d\- ]{4,14}$', 0,
                       'Пример допустимого телефонного номера: +7 (000) 000 00 00')
    index = Regexp('^\d{5,10}$', 0, 'Индекс введен не верно')


class ValidField:
    @staticmethod
    def email(placeholder=None):
        return StringField('', validators=[Validators.email], render_kw={
            'placeholder': placeholder if placeholder else 'Адрес почтового ящика',
            'pattern': Validators.email.regex.pattern,
            'required': ''})

    @staticmethod
    def text(placeholder=None, validators=None):
        return StringField(
            '', validators=validators,
            render_kw={
                'placeholder': placeholder,
                'pattern': validators[0].regex.pattern if validators else Validators.required.regex.pattern,
                'required': '',
            })

    @staticmethod
    def password(placeholder=None, repeat=False):
        return PasswordField(
            '', validators=[
                Validators.required if not repeat else EqualTo('password', message='Введенные пароли не совпадают')],
            render_kw={
                'placeholder': placeholder if placeholder else 'Пароль' if not repeat else 'Повторите пароль',
                'pattern': Validators.required.regex.pattern,
                'required': ''})


class LoginForm(FlaskForm):
    email = ValidField.email()
    password = ValidField.password()
    remember_me = BooleanField('Запомнить меня')
    submit = SubmitField('->Войти')


class RegistrationForm(FlaskForm):
    email = ValidField.email()
    password = ValidField.password()
    password2 = ValidField.password(repeat=True)
    submit = SubmitField('->Регистрация')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Пользователь с введенным адресом почтового ящика уже зарегистирован')


class ChangePasswordForm(FlaskForm):
    old_password = ValidField.password(placeholder='Старый пароль')
    password = ValidField.password(placeholder='Новый пароль')
    password2 = ValidField.password(repeat=True)
    submit = SubmitField('->Изменить пароль')


class PasswordResetRequestForm(FlaskForm):
    email = ValidField.email()
    submit = SubmitField('->Восстановить пароль')


class PasswordResetForm(FlaskForm):
    password = ValidField.password(placeholder='Новый пароль')
    password2 = ValidField.password(repeat=True)
    submit = SubmitField('->Восстановить пароль')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first() is None:
            raise ValidationError('Неизвестный почтовый адрес')


class ChangeEmailForm(FlaskForm):
    email = ValidField.email(placeholder='Новый почтовый ящик')
    password = ValidField.password()
    submit = SubmitField('->Изменить почтовый ящик')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Указанный почтовый ящик уже зарегистрирован')


class EditProfileForm(FlaskForm):
    surname = ValidField.text('Фамилия', validators=[Validators.name])
    name = ValidField.text('Имя', validators=[Validators.name])
    middle_name = ValidField.text('Отчество', validators=[Validators.name])

    telephone = ValidField.text('Номер телефона', validators=[Validators.telephone])

    submit = SubmitField('->Сохранить изменения')


class AddressForm(FlaskForm):
    country = ValidField.text('Страна', validators=[Validators.name])
    region = ValidField.text('Регион', validators=[Validators.name])
    city = ValidField.text('Город', validators=[Validators.name])
    address = ValidField.text('Адрес', validators=[Validators.name])
    index = ValidField.text('Индекс', validators=[Validators.index])

    submit = SubmitField('->Сохранить')
