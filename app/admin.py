import flask_login as login
from flask import request
from flask import url_for
from flask_admin.base import MenuLink
from flask_admin.contrib.sqlamodel import ModelView
from werkzeug.utils import redirect

from app import admin_manager, db
from app.auth.models.address import Address
from app.auth.models.user import User

from flask_admin import BaseView, expose


class BaseModelView(ModelView):
    create_modal = True
    edit_modal = True

    def is_accessible(self):
        return login.current_user.can()

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


class UserModelView(BaseModelView):
    excluded_list_columns = 'password_hash', 'addresses',
    form_excluded_columns = 'password_hash', 'addresses',

    can_create = False
    can_delete = False
    # can_edit = False


admin_manager.add_view(UserModelView(User, db.session, category='Аккаунт', name='Пользователи'))
admin_manager.add_view(BaseModelView(Address, db.session, category='Аккаунт', name='Адреса'))

# admin_manager.add_link(MenuLink(name='Войти', category='Аккаунт', endpoint='auth.login'))
# admin_manager.add_link(MenuLink(name='Выйти', category='Аккаунт', endpoint='auth.logout'))

admin_manager.add_link(MenuLink(name='GZad', url='/'))


class MyView(BaseView):
    @expose('/')
    def index(self):
        return self.render('analytics_index.html')
admin_manager.add_view(MyView(name='Ад2реса'))
