#!/usr/bin/env python
from app import create_app, db
from app.auth.models.user import User, Role
from app.auth.models.address import Address
from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand

app = create_app('default')
migrate = Migrate(app, db)
manager = Manager(app)


def make_shell_context():
    return dict(
        app=app,
        db=db,
        User=User,
        Address=Address,
        Permission=Role)
manager.add_command('shell', Shell(make_context=make_shell_context))

manager.add_command('db', MigrateCommand)


@manager.command
def test():
    """Запускаем модульные тесты."""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)

if __name__ == '__main__':
    manager.run()
