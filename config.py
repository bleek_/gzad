import os

basedir = os.path.abspath(os.path.dirname(__file__))


class MailConfig:
    MAIL_SERVER = 'smtp.yandex.ru'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = 'osu.oren@ya.ru'
    MAIL_PASSWORD = 'oren.osu'
    MAIL_SUBJECT_PREFIX = '[GZad]'
    MAIL_SENDER = 'GZad <osu.oren@ya.ru>'


class Config(MailConfig):
    SECRET_KEY = '1234'
    CSRF_ENABLED = True

    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

    BABEL_DEFAULT_LOCALE = 'ru'

    @staticmethod
    def init_app(app):
        pass


class DevConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'dev.sqlite')


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'test.sqlite')


class ProdConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'prod.sqlite')


config = {
    'development': DevConfig,
    'testing': TestConfig,
    'production': ProdConfig,

    'default': DevConfig
}
